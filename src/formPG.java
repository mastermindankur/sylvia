import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.util.*;

public class formPG extends JFrame
{
UniversityOfBedfordshire main = new UniversityOfBedfordshire();
public static void main(String args[]){
formPG frame = new formPG();

frame.createUI(frame); 
}
public void createUI(formPG frame)
{
frame.setSize(500,310);
frame.setLocationRelativeTo(null);
frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
frame.setVisible(true);
}
public formPG(){
JPanel jpformPG = new JPanel();
//jpformPG.setLocation(500,400);

jpformPG.setLayout(new GridLayout(4,0,10,10));
jpformPG.add(new JLabel("      Name"));
final JTextField jtName = new JTextField(10);
jpformPG.add(jtName);
JPanel jpUp = new JPanel(new GridLayout(2,1,10,10));
jpUp.add(new JLabel(" "));
add(jpUp,BorderLayout.NORTH);
JPanel jpD = new JPanel(new GridLayout(2,1,10,10));
jpD.add(new JLabel(" "));
add(jpD,BorderLayout.WEST);
JPanel jpW = new JPanel(new GridLayout(2,1,10,10));
jpW.add(new JLabel(" "));
add(jpW,BorderLayout.EAST);
JPanel jpB = new JPanel(new GridLayout(2,1,10,10));
jpUp.add(new JLabel(" "));
add(jpUp,BorderLayout.NORTH);
jpformPG.add(new JLabel("      Email"));
jpformPG.setFont(new Font("Arial",Font.BOLD,15));
final JTextField jtEmail = new JTextField(10);
jpformPG.add(jtEmail);
jpformPG.add(new JLabel("      Comment"));
jpformPG.setFont(new Font("TimesNewRoman",Font.BOLD,22));
final JTextField jtComment = new JTextField(10);
jpformPG.add(jtComment);
jpformPG.add(new JLabel("     "));
JButton jbSubmit = new JButton("Submit");
JButton jbCancel  = new JButton("Cancel");
JPanel jpSub = new JPanel();
jpSub.add(jbSubmit,BorderLayout.EAST);
jpSub.add(jbCancel,BorderLayout.WEST);
jpUp.setBackground(new Color(255,255,255));
jpformPG.setBackground(new Color(255,255,255));
jpSub.setBackground(new Color(255,255,255));
//jpformPG.setSize(400,400);
jbSubmit.addActionListener(new ActionListener(){
public void actionPerformed(ActionEvent e){
String name = jtName.getText();
String email = jtEmail.getText();
String comment = jtComment.getText();
try{
File file = new File("PG.txt");
String str="";
if(!file.exists()){
file.createNewFile();
}
else
{
Scanner s = new Scanner(file);

while(1==1)
{
String temp = s.nextLine();
if(temp.equals("E"))
break;
else
str+=temp+"\r\n";
}
s.close();
}
str+=name+" "+email+" "+comment+"\r\n"+"E";
FileWriter fw=new FileWriter(file);

BufferedWriter bw=new BufferedWriter(fw);
bw.write(str);
bw.close();
}
catch(Exception ex)
{
}
main.createAndShowUI();
setVisible(false);
}
});
jbCancel.addActionListener(new ActionListener(){
public void actionPerformed(ActionEvent e){
main.createAndShowUI();
setVisible(false);
}
});
add(jpformPG,BorderLayout.CENTER);
add(jpSub,BorderLayout.SOUTH);
}
}