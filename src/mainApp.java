import java.awt.*;  
import javax.swing.*;  
import javax.swing.border.BevelBorder;  
import java.io.*;
import java.util.*;
import java.awt.event.*;
  
public class mainApp {  
   private static final int BUTTON_COUNT = 10;  
   private static final Color GRADIENT_COLOR2 = new Color(255,255,255);  
   private static final float G_X2 = 50;  
   private static final float G_Y2 = 50;  
   private static Color TRANSPARENT = new Color(104, 185, 10, 45);  
  
  // form UGform = new form();
  // formPG PGform = new formPG();
   private JPanel mainPanel = new JPanel(new GridBagLayout()) {  
      @Override  
      protected void paintComponent(Graphics g) {  
         super.paintComponent(g);  
  
         Paint paint = new GradientPaint(0f, 0f, Color.white, G_X2, G_Y2,  
                  GRADIENT_COLOR2, true);  
         Graphics2D g2 = (Graphics2D) g;  
         g2.setPaint(paint);  
         g2.fillRect(0, 0, getWidth(), getHeight());  
         g.setColor(new Color(175,211,31));
         JButton jbUG = new JButton("UG");
        // jbUG.setLocation(350,350);
         jbUG.setToolTipText("Undergraduate");
         JPanel jpU = new JPanel();
         jpU.add(jbUG,BorderLayout.WEST);
         jpU.setLocation(350,350);
         add(new JLabel("      "));
         add(jpU);
         JButton jbPG = new JButton("PG");
      //   jbUG.setLocation(100,200);
         jbPG.setToolTipText("Postgraduate");
         JPanel jpP = new JPanel();
         add(new JLabel("                                                       "));
         jpP.add(jbPG,BorderLayout.EAST);
        
         jpP.setLocation(300,200);
         add(jpP);
         
         jbUG.addActionListener(new ActionListener(){
             public void actionPerformed(ActionEvent e){
                 offerInfoUG UGform = new offerInfoUG();
                 //setVisible(false);
                UGform.createUI(UGform);
                }
            });
            
            jbPG.addActionListener(new ActionListener(){
             public void actionPerformed(ActionEvent e){
                 
                //setVisible(false);
                 offerInfoPG PGform = new offerInfoPG();
                 
                PGform.createUI(PGform);
                }
            });
         
         int cnt=0,cnt1=0;
         try{
             Scanner s = new Scanner(new File("UG.txt"));
             
             while(1==1)
             {
                 String str = s.nextLine();
                 if(str.equals("E"))
                 break;
                 else
                 cnt++;
                }
                s.close();
            }
            catch(Exception ex)
            {}
            try{
                 Scanner s1 = new Scanner(new File("PG.txt"));
             
             while(1==1)
             {
                 String str1 = s1.nextLine();
                 if(str1.equals("E"))
                 break;
                 else
                 cnt1++;
                }
                s1.close();
            }
            catch(Exception ex)
            {}
for(int i=0;i<=cnt;i++)
g.drawOval(560-10*i,350-10*i,10+20*i,10+20*i);
g.setColor(new Color(216,80,216));
for(int i=0;i<=cnt1;i++)
g.drawOval(900-10*i,350-10*i,10+20*i,10+20*i);

      }  
   };  
   
  
   
  
   public mainApp() {  
      int mpeb = 80;  
      mainPanel.setBorder(BorderFactory.createEmptyBorder(mpeb, 3 * mpeb, mpeb,  
               3 * mpeb));  
      int ipeb = 25;  
  
      GridBagConstraints gbc = new GridBagConstraints();   
   }  
  
   public JComponent getComponent() {  
      return mainPanel;  
   }  
  
   public static void createAndShowUI() {  
      JFrame  frame = new JFrame("mainApp");  
      frame.setSize(1200,710);
      frame.getContentPane().add(new mainApp().getComponent());  
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
     // frame.pack();  
      frame.setLocationRelativeTo(null);  
      frame.setVisible(true);  
   }  
  
   public static void main(String[] args) {  
      java.awt.EventQueue.invokeLater(new Runnable() {  
         public void run() {  
            createAndShowUI();  
         }  
      });  
   }  
}