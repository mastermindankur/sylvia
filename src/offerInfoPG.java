import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;

public class offerInfoPG extends JFrame{
formPG Wform = new formPG();
UniversityOfBedfordshire main = new UniversityOfBedfordshire();
public static void main(String args[]){
offerInfoPG frame = new offerInfoPG();
frame.createUI(frame);
}
public void createUI(offerInfoPG frame){
frame.setSize(500,310);
frame.setLocationRelativeTo(null);
frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
frame.setVisible(true);
}
public offerInfoPG()
{
JPanel jpOffer = new JPanel(new GridLayout(1,1,10,10));
//jLabel jlb = new JLabel("  ");
//jlb.setFont(new Font("TimesNewRoman",Font.BOLD,15));
//jpOffer.add(jlb,BorderLayout.CENTER);
JLabel jlbl = new JLabel("  We Offer");
jlbl.setFont(new Font("Tahoma",Font.BOLD,18));
jpOffer.add(jlbl);
//jpOffer.add(new JLabel("  "),BorderLayout.SOUTH);
JPanel jpList = new JPanel(new GridLayout(3,1,10,10));
ImageIcon icon  = new ImageIcon("dot.jpg");
jpList.add(new JLabel(" "));
JLabel jl1 = new JLabel("      MA in Art and Design",icon,SwingConstants.LEFT);
//jl1.setFont(new Font("TimesNewRoman",Font.BOLD,17));
jpList.add(jl1);
JLabel jl2 = new JLabel("      MA in Fashion Styling and Promotion",icon,SwingConstants.LEFT);
//jl2.setFont(new Font("TimesNewRoman",Font.BOLD,17));
jpList.add(jl2);

jpOffer.setFont(new Font("Tahoma",Font.BOLD,10));

//setLayout(new BorderLayout());
setLayout(new BorderLayout());
jpList.setLayout(new GridLayout(9,2,10,10));

JPanel jpWish = new JPanel();
JLabel jlWish = new JLabel("Do you wish to submit your e-mail for more information?            ");
jpWish.add(jlWish,BorderLayout.EAST);
jpList.setBackground(new Color(255,255,255));
jpOffer.setBackground(new Color(255,255,255));
jpWish.setBackground(new Color(255,255,255));
JButton jbYes = new JButton("Yes");
JButton jbNo = new JButton("No");
//JPanel jpSouth = new JPanel(new BorderLayout());
jbYes.addActionListener(new ActionListener(){
public void actionPerformed(ActionEvent e){
Wform.createUI(Wform);
setVisible(false);
}
});
jbNo.addActionListener(new ActionListener(){
public void actionPerformed(ActionEvent e){
main.createAndShowUI();
setVisible(false);
}
});
//jbYes.setFont(new Font("Sans",Font.BOLD,20));
//jbYes.setToolTipText("Undergraduate");
jbYes.setLayout(new FlowLayout(FlowLayout.CENTER));
jpWish.add(jbYes,BorderLayout.WEST);
jpWish.add(jbNo,BorderLayout.EAST);
//jpSo
add(jpOffer,BorderLayout.NORTH);
add(jpList,BorderLayout.CENTER);
add(jpWish,BorderLayout.SOUTH);
}
}
 