import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;

public class offerInfoUG extends JFrame{
form Wform = new form();
UniversityOfBedfordshire main = new UniversityOfBedfordshire();
public static void main(String args[]){
offerInfoUG frame = new offerInfoUG();
frame.createUI(frame);
}
public void createUI(offerInfoUG frame){
frame.setSize(500,310);
frame.setLocationRelativeTo(null);
frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
frame.setVisible(true);
}
public offerInfoUG()
{
JPanel jpOffer = new JPanel(new GridLayout(0,1,10,10));
JLabel jlbl = new JLabel("  We Offer");
jlbl.setFont(new Font("Tahoma",Font.BOLD,15));
jpOffer.add(jlbl);
JPanel jpList = new JPanel();
ImageIcon icon  = new ImageIcon("dot.jpg");
jpList.add(new JLabel(" "));
JLabel jl1 = new JLabel("    BA(Hons) Fashion Design",icon,SwingConstants.LEFT);
jpList.add(jl1);
JLabel jl2 = new JLabel("    BA(Hons) Advertising Design",icon,SwingConstants.LEFT);
jpList.add(jl2);
JLabel jl3 = new JLabel("    BA(Hons) Animation",icon,SwingConstants.LEFT);
jpList.add(jl3);
JLabel jl4 = new JLabel("    BA(Hons) Art and Design",icon,SwingConstants.LEFT);
jpList.add(jl4);
JLabel jl5 = new JLabel("    BA(Hons) Fine Art",icon,SwingConstants.LEFT);
jpList.add(jl5);
JLabel jl6 = new JLabel("    BA(Hons) Graphic Design",icon,SwingConstants.LEFT);
jpList.add(jl6);
JLabel jl7 = new JLabel("    BA(Hons) Illustration",icon,SwingConstants.LEFT);
jpList.add(jl7);
JLabel jl8 = new JLabel("    BA(Hons) Interior Architecture",icon,SwingConstants.LEFT);
jpList.add(jl8);
JLabel jl9 = new JLabel("    BA(Hons) Interior Design",icon,SwingConstants.LEFT);
jpList.add(jl9);
JLabel jl10 = new JLabel("    BA(Hons) Photography and Video Art",icon,SwingConstants.LEFT);
jpList.add(jl10);
jpOffer.setFont(new Font("Sans",Font.BOLD,20));
setLayout(new BorderLayout());
jpList.setLayout(new GridLayout(11,2,10,10));
JPanel jpWish = new JPanel();
JLabel jlWish = new JLabel("Do you wish to submit your e-mail for more information?              ");
jpWish.add(jlWish,BorderLayout.NORTH);
JButton jbYes = new JButton("Yes");
JButton jbNo = new JButton("No");
jpList.setBackground(new Color(255,255,255));
jpOffer.setBackground(new Color(255,255,255));
jpWish.setBackground(new Color(255,255,255));
jbYes.addActionListener(new ActionListener(){
public void actionPerformed(ActionEvent e){
Wform.createUI(Wform);
setVisible(false);
}
});
jbNo.addActionListener(new ActionListener(){
public void actionPerformed(ActionEvent e){
main.createAndShowUI();
setVisible(false);
}
});
//jbYes.setFont(new Font("Sans",Font.BOLD,20));
//jbYes.setToolTipText("Undergraduate");
jpWish.add(jbYes,BorderLayout.WEST);
jpWish.add(jbNo,BorderLayout.EAST);
add(jpOffer,BorderLayout.NORTH);
add(jpList,BorderLayout.CENTER);
add(jpWish,BorderLayout.SOUTH);
}
}
 